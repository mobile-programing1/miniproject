import 'dart:io';
import 'dart:math';

class Player {
  late String name;
  late int heart;
  late int damage;

  String getName(){
    return name;
  }

  void setName(String name){
    this.name;
  }

  int getHeart(){
    return heart;
  }

  void setHeart(int heart){
    this.heart;
  }

  int getDamage() {
    return damage;
  }

  void setDamage(int damage){
    this.damage;
  }
}

class Adventure extends Player{
  Adventure(){
    name = 'Adventure';
    heart = 1;
    damage = 2;
    
    String getName(){
      return name;
    }
    
    void setName(String name){
      this.name;
    }
      
    int getHeart(){
      return heart;
    }
        
    void setHeart(int heart){
      this.heart;
    }
    
    int getDamage() {
      return damage;
    }

    void setDamage(int damage){
      this.damage;
    }
  }
}

class Apothecary extends Player{
  Apothecary(){
    name = 'Apothecary';
    heart = 3;
    damage = 1;
    String getName(){
      return name;
    }
    
    void setName(String name){
      this.name;
    }
      
    int getHeart(){
      return heart;
    }
        
    void setHeart(int heart){
      this.heart;
    }
    
    int getDamage() {
      return damage;
    }

    void setDamage(int damage){
      this.damage;
    }
  }
}

class Prisoner extends Player{
  Prisoner(){
    name = 'Prisoner';
    heart = 1;
    damage = 1;
    String getName(){
      return name;
    }
    
    void setName(String name){
      this.name;
    }
      
    int getHeart(){
      return heart;
    }
        
    void setHeart(int heart){
      this.heart;
    }
    
    int getDamage() {
      return damage;
    }

    void setDamage(int damage){
      this.damage;
    }
  }
}

class Monster {
  int blood = 2;
  int hit = 2;
  String status = 'ALIVE';

  int getBlood(){
    return blood;
  }

  void setBlood(int blood) {
    if(blood == 0){
      status = 'DIED';
    }
  }

  int getHit(){
    return hit;
  }

  void setHit(int hit){
    this.hit;
  }
  String getStatus() {
    return status;
  }

  void setStatus(String status){
    this.status;
  }
}

class Quiz {
  List<int> numbers = [Random().nextInt(10), Random().nextInt(10)];
  String? operation;
  int? answer;

  Quiz() {
    final List<String> operations = ['+', '-', '*'];
    int? ans;
    List<int> nums = numbers;
    // generate a random operation
    int opIdx = Random().nextInt(3);
    switch (opIdx) {
      case 0:
        ans = nums[0] + nums[1];
        break;
      case 1:
        ans = nums[0] - nums[1];
        break;
      case 2:
        ans = nums[0] * nums[1];
        break;
    }
    operation = operations[opIdx];
    answer = ans;
  }
  // String expression() => "${numbers[0]} $operation ${numbers[1]} = ?";

  getNum() {
    return numbers;
  }

  getOperation() {
    return operation;
  }

  getResult() {
    return answer;
  }

  @override
  String toString() {
    return '${numbers[0]} $operation ${numbers[1]} = ?';
  }
}

class Game {
  Player player = Player();
  Monster monster = Monster();
    int hpMon = 0;
    int hpMan = 0;
   int chosenChar(){
    int select = 0;
    print("Select your character 1.Adventure 2.Apothecary 3.Prisoner");
    select = int.parse(stdin.readLineSync()!);
    switch (select) {
        case 1:
          player = Adventure();
          print("Now you will be Adventure!");
          break;
        case 2:
          player = Apothecary();
          print("Now you will be Apothecory!");
          break;
        case 3:
          player = Prisoner();
          print("Now you will be Prisoner!");
          break;
        default:
      }
    return select;
  }

  void showWelcome(){
    print("Welcome to who will be Monster game");
  }

    void showDefault() {
    print('status : ${player.getName()}');
    print('My Heart : ${player.getHeart()}');
    print('My Damage : ${player.getDamage()}');
    print('\n');
    print('Monster will coming..');
    print('Monster Blood : ${monster.getBlood()}');
    print('HIT : ${monster.getHit()}');
  }

  void showSolution() {
    print('Please input your answer for damage to Monster!');
      Quiz quiz = Quiz();
      print(quiz.toString());
      int inputAns = int.parse(stdin.readLineSync()!);
      if(inputAns == quiz.getResult()){
          hpMon = monster.getBlood() - player.getDamage();
        print('now hp Monster $hpMon');
      } else {
          hpMan = player.getHeart() - monster.getHit();
          print('now hp Player $hpMan');
      }
}
  void showResult(){
    if(hpMan < hpMon){
      print('YOU DIED');
      print('\n');
      print('The game is ending..');
    } else if(hpMon <= 0){
      print('Congrat! You can defeat monster');
      print('Now you will be Monster.. HAHA');
      print('\n');
      print('The game is ending..');
    }
  }
}

void main() {
  Game game = Game();
  game.showWelcome();
  game.chosenChar();
  game.showDefault();
  game.showSolution();
  game.showResult();
}